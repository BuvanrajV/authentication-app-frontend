import axios from "axios";

const baseURL = "https://authentication-app-a1ny.onrender.com/user";

export function signupApi(email, password) {
  return axios.post(`${baseURL}/signup`, {
    email,
    password,
  });
}

export function getTokenApi(email, password) {
  return axios.post(`${baseURL}/login`, {
    email,
    password,
  });
}

export function getUserDetailsApi (token){
    return axios.get(`${baseURL}/login`,{
        headers: {
            Authorization : `Bearer ${token}`
        }
    })
}

export function updateDetailsApi (id,details){
    return axios.put(`${baseURL}/${id}`,details)
}