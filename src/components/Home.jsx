import { Flex, Button } from "@chakra-ui/react";
import { Link } from "react-router-dom";

function Home() {
  return (
    <section>
      <Flex
        flexDirection="column"
        gap="3vh"
        height="100vh"
        width="100vw"
        justify="center"
        align="center"
      >
        <div>
          <Link to="/signup">
            <Button backgroundColor="whitesmoke">Signup</Button>
          </Link>
        </div>
        <div>
          <Link to="login">
            <Button backgroundColor="whitesmoke">Login</Button>
          </Link>
        </div>
      </Flex>
    </section>
  );
}

export default Home;
