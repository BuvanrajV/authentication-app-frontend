import { GETUSERDETAILS } from "./userDetailsActionTypes";

function userDetailsReducer(state, action) {
  switch (action.type) {
    case GETUSERDETAILS:
      return action.userDetails;
    default:
      return state;
  }
}

export default userDetailsReducer;
