import { useState } from "react";
import { Link } from "react-router-dom";
import { signupApi } from "../api";
import {
  Flex,
  Input,
  InputGroup,
  InputLeftElement,
  Button,
  InputRightElement,
} from "@chakra-ui/react";
import { AiOutlineMail } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";

function Signup() {
  const [email, setEmail] = useState("");
  const [isEmailExist, setIsEmailExist] = useState(false);
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  const handleSignup = (event) => {
    event.preventDefault();
    signupApi(email, password)
      .then((res) => {
        setEmail("");
        setPassword("");
        setIsLogin(true);
        console.log(res.data);
      })
      .catch((err) => {
        console.error(err.response.data);
        setIsEmailExist(true);
      });
  };
  return (
    <section className="signup-section">
      <form onSubmit={handleSignup}>
        <Flex
          flexDirection="column"
          gap="3vh"
          justify="center"
          w="100vw"
          h="100vh"
          align="center"
        >
          <div style={{fontSize:"1.8rem", fontWeight:"700"}}>SIGNUP</div>
          <div>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={<AiOutlineMail color="gray.300" />}
              />
              <Input
                placeholder="Email"
                onChange={(e) => {
                  setIsEmailExist(false);
                  setEmail(e.target.value);
                }}
                value={email}
              />
            </InputGroup>
            {isEmailExist && (
              <div style={{ color: "red", fontSize: "0.8rem" }}>
                Email already exist
              </div>
            )}
          </div>
          <div>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={<RiLockPasswordLine color="gray.300" />}
              />
              <Input
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={() => setShow(!show)}>
                  {show ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
          </div>
          <div>
            <Button type="submit">Signup</Button>
          </div>
          {isLogin && (
            <Flex gap="3vw" mt="2vh">
              <div style={{ fontSize: "1rem" }}>Signup Successfull !</div>
              <div>
                <Link to="/login">
                  <Button colorScheme="teal" size="xs">
                    Login
                  </Button>
                </Link>
              </div>
            </Flex>
          )}
        </Flex>
      </form>
    </section>
  );
}

export default Signup;
