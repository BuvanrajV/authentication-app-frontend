import { useContext, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import {
  Flex,
  Button,
  TableContainer,
  Table,
  Tbody,
  Tr,
  Td,
} from "@chakra-ui/react";
import { UserDetailsContext } from "../App";
import { GETUSERDETAILS } from "./reducers/userDetailsActionTypes";

function UserDetails() {
  const userDetailsContext = useContext(UserDetailsContext);

  const location = useLocation();

  useEffect(() => {
    userDetailsContext.userDetailsDispatch({
      type: GETUSERDETAILS,
      userDetails: location.state[0],
    });
  });

  const userDetails = userDetailsContext.userDetails;

  !userDetails.name && (userDetails.name = "--");
  !userDetails.bio && (userDetails.bio = "--");
  !userDetails.phone && (userDetails.phone = "--");

  return (
    <section>
      <Flex justifyContent="flex-end">
        <Link to="/login">
          <Button>Logout</Button>
        </Link>
      </Flex>
      <Flex justify="center" mt="15vh">
        <Flex flexDirection="column" gap="3vh">
          <Flex justify="space-between">
            <div>Profile</div>
            <div>
              <Link to={`/editDetails/${userDetails.id}`}>
                <Button>Edit</Button>
              </Link>
            </div>
          </Flex>
          <TableContainer>
            <Table variant="simple">
              <Tbody>
                <Tr>
                  <Td>Name</Td>
                  <Td>{userDetails.name}</Td>
                </Tr>
                <Tr>
                  <Td>Bio</Td>
                  <Td>{userDetails.bio}</Td>
                </Tr>
                <Tr>
                  <Td>Phone</Td>
                  <Td>{userDetails.phone}</Td>
                </Tr>
              </Tbody>
            </Table>
          </TableContainer>
        </Flex>
      </Flex>
    </section>
  );
}

export default UserDetails;
