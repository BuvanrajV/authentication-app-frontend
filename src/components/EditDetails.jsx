import { useState } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Flex, Text, Input, Button } from "@chakra-ui/react";
import { updateDetailsApi } from "../api";

function EditDetails() {
  const params = useParams();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [bio, setBio] = useState("");
  const [phone, setPhone] = useState("");
  const [update, setUpdate] = useState({});
  const [isUpdated, setIsUpdated] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    updateDetailsApi(params.id, update)
      .then((res) => {
        setName("");
        setBio("");
        setPhone("");
        setIsUpdated(true);
        navigate("/login");
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <section>
      <Flex justifyContent="flex-end">
        <Link to="/login">
          <Button>Logout</Button>
        </Link>
      </Flex>
      <form onSubmit={handleSubmit}>
        <Flex mt="15vh" flexDirection="column" align="center" gap="3vh">
          <div>Change Info</div>
          <div>
            <Text mb="8px">Name:</Text>
            <Input
              value={name}
              variant="outline"
              placeholder="Enter Your Name"
              onChange={(e) => {
                setIsUpdated(false);
                setName(e.target.value);
                setUpdate({ ...update, name });
              }}
            />
          </div>
          <div>
            <Text mb="8px">Bio:</Text>
            <Input
              value={bio}
              variant="outline"
              placeholder="Enter Your Bio"
              onChange={(e) => {
                setIsUpdated(false);
                setBio(e.target.value);
                setUpdate({ ...update, bio });
              }}
            />
          </div>
          <div>
            <Text mb="8px">Phone:</Text>
            <Input
              value={phone}
              variant="outline"
              placeholder="Enter Your Phone"
              onChange={(e) => {
                setIsUpdated(false);
                setPhone(e.target.value);
                setUpdate({ ...update, phone });
              }}
            />
          </div>
          <div>
            <Button type="submit">Save</Button>
          </div>
          {isUpdated && (
            <div style={{ fontSize: "1rem", color: "blue" }}>
              Updated Successfully !
            </div>
          )}
        </Flex>
      </form>
    </section>
  );
}

export default EditDetails;
