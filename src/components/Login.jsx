import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { getTokenApi, getUserDetailsApi } from "../api";
import {
  Flex,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Button,
} from "@chakra-ui/react";
import { AiOutlineMail } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const [isEmailWrong, setIsEmailWrong] = useState(false);
  const [isPasswordWrong, setIsPasswordWrong] = useState(false);
  const [isLoginFailed, setIsLoginFailed] = useState(false);

  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsLoginFailed(false);
    getTokenApi(email, password)
      .then((res) => {
        return res.data;
      })
      .then((token) => getUserDetailsApi(token.accessToken))
      .then((res) => {
        setEmail("");
        setPassword("");
        navigate("/userDetails",{state : res.data});
      })
      .catch((err) => {
        console.log(err);
        if (err.response.data.message.includes("Email")) {
          setIsEmailWrong(true);
        } else if (err.response.data.message.includes("Password")) {
          setIsPasswordWrong(true);
        } else {
          console.error(err);
          setIsLoginFailed(true);
        }
      });
  };
  return (
    <section className="signup-section">
      <form onSubmit={handleSubmit}>
        <Flex
          flexDirection="column"
          gap="3vh"
          justify="center"
          w="100vw"
          h="100vh"
          align="center"
        >
          <div style={{ fontSize: "1.8rem", fontWeight: "700" }}>LOGIN</div>
          <div>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={<AiOutlineMail color="gray.300" />}
              />
              <Input
                type="tel"
                placeholder="Email"
                onChange={(e) => {
                  setEmail(e.target.value);
                  setIsEmailWrong(false);
                }}
                value={email}
              />
            </InputGroup>
            {isEmailWrong && (
              <div style={{ color: "red", fontSize: "0.8rem" }}>
                Wrong Email!
              </div>
            )}
          </div>
          <div>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={<RiLockPasswordLine color="gray.300" />}
              />
              <Input
                type={show ? "text" : "password"}
                placeholder="Enter password"
                onChange={(e) => {
                  setPassword(e.target.value);
                  setIsPasswordWrong(false);
                }}
                value={password}
              />
              <InputRightElement width="4.5rem">
                <Button h="1.75rem" size="sm" onClick={() => setShow(!show)}>
                  {show ? "Hide" : "Show"}
                </Button>
              </InputRightElement>
            </InputGroup>
            {isPasswordWrong && (
              <div style={{ color: "red", fontSize: "0.8rem" }}>
                Wrong Password!
              </div>
            )}
          </div>
          <div>
            <Button type="submit">Login</Button>
          </div>
          {isLoginFailed && (
            <Flex gap="3vw" mt="2vh">
              <div style={{ fontSize: "1rem", color: "red" }}>
                Login Failed !
              </div>
            </Flex>
          )}
        </Flex>
      </form>
    </section>
  );
}

export default Login;
