import { useReducer } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";
import userDetailsReducer from "./components/reducers/userDetailsReducer";
import Home from "./components/Home";
import Signup from "./components/Signup";
import Login from "./components/Login";
import UserDetails from "./components/UserDetails";
import EditDetails from "./components/EditDetails";
import React from "react";

export const UserDetailsContext = React.createContext();

function App() {
  const [userDetails, userDetailsDispatch] = useReducer(userDetailsReducer, []);
  return (
    <UserDetailsContext.Provider value={{ userDetails, userDetailsDispatch }}>
      <ChakraProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/login" element={<Login />} />
            <Route path="/userDetails" element={<UserDetails />} />
            <Route path="/editDetails/:id" element={<EditDetails />} />
          </Routes>
        </BrowserRouter>
      </ChakraProvider>
    </UserDetailsContext.Provider>
  );
}

export default App;
